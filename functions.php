<?php

//função migalhas ou breadcrumb
require_once ('inc/breadcrumb.php');

//lightbox das imagens
require_once ('inc/lightbox.php');

//Paginação
require_once ('inc/pagination.php');

//Habilita imagens destaques
add_theme_support( 'post-thumbnails' );

//função Custom CSS
require_once ('inc/simple-custom-css.php');

//função para customização do template
require_once ('inc/customizacao.php');

//Widgets
require_once ('inc/widgets.php');

//função para mostrar post por shortcode
require_once ('inc/add_posts.php');

//Registra o menu principal
register_nav_menus(
	array(
		'menu-principal' => 'Menu Principal',
	));
	
//função menu bootstrap
require_once ('inc/wp_bootstrap_navwalker.php');

//restringe uploads
require_once ('inc/restrict_uploads.php');
