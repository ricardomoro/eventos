<?php get_header(); ?>

<?php breadcrumb(); ?>

<div class="container" id="conteudo">
    <div class="row">
        <div class="col-xs-12">
           <section class="conteudo box">
                <h2>
                    <?php $category = get_the_category(); if($category[0]){ 
                        echo '<a href="'.get_category_link($category[0]->term_id ).'">'.$category[0]->cat_name.'</a>';}
                    ?>
                </h2>
                <?php while (have_posts()) : the_post(); ?>
                    <article>
                        <?php if ( has_post_thumbnail()) :  ?>
                            <div>
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                                    <?php the_post_thumbnail('thumbnail'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                        <h3>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title();?>"><?php the_title();?></a>
                        </h3>
                        <?php the_excerpt(); ?>
                        <small>
                            <?php the_time('j'); ?> de <?php the_time('F'); ?> de <?php the_time('Y'); ?>
                        </small>
                        <div class="linha"></div>
                    </article>
                <?php endwhile; ?>
                <p class="center"><?php custom_pagination(); ?></p>
                <div class="col-xs-12 separador"></div>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>
