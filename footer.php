    <a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do Conte&uacute;do</a>
    <section id="widget-rodape">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if (!dynamic_sidebar('rodape')) : ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <a href="http://www.ifrs.edu.br/" title="Site do IFRS">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-ifrs-branco.png" alt="Site do IFRS" />
                    </a>
                </div>
                
                <div class="col-md-4 center">
                    <p class="titulo"><strong><?php bloginfo('name'); ?></strong></p>
                    <p><?php bloginfo('description'); ?></p>
                </div>
                
                <div class="col-md-4 col-lg-3 col-lg-offset-1">
                    <div class="center links">
                        <!-- Wordpress -->
                        <p>
                            <a href="http://br.wordpress.org/" target="blank">Desenvolvido com Wordpress<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                        </p>
                        <!-- GPLv3 -->
                        <p>
                            <a href="https://bitbucket.org/ricardomoro/eventos/" target="blank">C&oacute;digo-fonte deste tema sob a Licen&ccedil;a GPLv3<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                        </p>
                        <!-- Creative Commons 4.0 -->
                        <p>
                            <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc-by-nc-nd.png" alt="M&iacute;dia licenciada sob a Licen&ccedil;a Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional (abre uma nova p&aacute;gina)" /></a>
                            <span class="glyphicon glyphicon-new-window"></span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <?php wp_footer(); ?>

    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>

    <script src="http://barra.brasil.gov.br/barra.js?cor=verde" type="text/javascript" async="async"></script>
</body>
</html>
