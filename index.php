<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="conteudo">
    <div class="row">
    <?php if ( is_active_sidebar('sidebar') ): ?>
        <div class="col-md-8">
        <?php while ( have_posts() ) : the_post(); ?>
            <article>
                <h2><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h2>

                <!-- Insere a imagem destaque na notícia, caso tenha. -->
                <?php if ( has_post_thumbnail()): ?>
                    <div style="float:left;margin: 0 1.75em 2.5em 0;">
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <a class="fancybox" rel="gallery1" href="<?php echo $url; ?>">
                            <?php the_post_thumbnail( array(320,225) ); ?>
                        </a>
                    </div>
                <?php endif; ?>
                <!-- FIM - Insere a imagem destaque na notícia, caso tenha. -->

                <?php the_excerpt(); ?>
            </article>
        <?php endwhile;?>
        </div>

        <div class="col-md-4">
            <aside>
                <?php if ( ! dynamic_sidebar( 'sidebar' ) ) : endif; ?>
            </aside>
        </div>
    <?php else: ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-xs-12">
                <article>
                    <h1><a href="<?php echo get_permalink() ?>"><?php the_title(); ?></a></h1>
                    
                    <!-- Insere a imagem destaque na notícia, caso tenha. -->
                    <?php if ( has_post_thumbnail()): ?>
                        <div style="float:left;margin: 0 1.75em 2.5em 0;">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <a class="fancybox" rel="gallery1" href="<?php echo $url; ?>">
                                <?php the_post_thumbnail( array(320,225) ); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <!-- FIM - Insere a imagem destaque na notícia, caso tenha. -->
                    
                    <?php the_excerpt(); ?>
                </article>
            </div>
        <?php endwhile;?>
    <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>
