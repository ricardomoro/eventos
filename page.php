<?php get_header(); ?>

<?php breadcrumb(); ?>

<div class="container" id="conteudo">
	<section class="row">
		<?php if ( is_active_sidebar('sidebar') ): ?>
            <article class="col-md-8">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                <?php endwhile; ?>
            </article>

            <aside class="col-md-4">
                <?php if ( ! dynamic_sidebar( 'sidebar' ) ) : endif; ?>
            </aside>
		<?php else: ?>
    		<article class="col-xs-12">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                <?php endwhile; ?>
			</article>
		<?php endif; ?>
	</section>
</div>

<?php get_footer(); ?>
