<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Título -->
    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title( '-', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        $site_description = get_bloginfo( 'description', 'display' );
        if ( $site_description && ( is_home() || is_front_page() ) )
            echo ' - '.$site_description;

        // Add a page number if necessary:
        if ( $paged >= 2 || $page >= 2 )
            echo ' - ' . sprintf( 'Página %s', max( $paged, $page ) );
    ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon"        type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico"/>

    <!-- Google Fonts - Open Sans -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- CSS -->
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" />
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-accessibility.css" />
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/style.css" />
    <link rel="alternate stylesheet" type="text/css"     href="<?php echo get_template_directory_uri(); ?>/style-contraste.css" title="contraste" />
    <link rel="stylesheet"           type="text/css"     href="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.css?v=2.1.4"  media="screen" />
    
    <!-- JS -->
    <!--[if lt IE 9]>
        <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->
    
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-2.1.0.min.js"              type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"                 type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-accessibility.min.js"   type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/styleswitcher.js"                 type="text/javascript"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <!-- Barra do Governo -->
    <div id="barra-brasil">
        <a href="http://brasil.gov.br">Portal do Governo Brasileiro</a>
    </div>

    <!-- Barra de Acessibilidade -->
    <div class="container">
        <div class="row" id="atalhos">
            <div class="col-xs-6 col-md-4">
                <a href="/" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-globe"></span> <span class="sr-only">Ir para o </span>Portal de Eventos</a>
            </div>
            <div class="col-md-5 hidden-xs hidden-sm">
                <ul class="pull-right">
                    <li><a href="#conteudo"                 accesskey="1"><span class="sr-only">Ir para o</span> Conteúdo <span class="badge">1</span></a></li>
                    <li><a href="#menu-principal"           accesskey="2"><span class="sr-only">Ir para o</span> Menu <span class="badge">2</span></a></li>
                    <li><a href="#search-acessibilidade"    accesskey="3"><span class="sr-only">Ir para a</span> Pesquisa <span class="badge">3</span></a></li>
                    <li><a href="#contraste-switch"         accesskey="4" id="contraste-switch"><span class="glyphicon glyphicon-adjust"></span> <span class="sr-only">Alterar </span>Contraste <span class="badge">4</span></a></li>
                </ul>
            </div>
            <div class="col-xs-6 col-md-3">
                <form method="get" action="<?php bloginfo('url'); ?>/" class="form-inline pull-right" role="search">
                    <div class="form-group">
                        <label for="search-acessibilidade" class="sr-only">Buscar...</label>
                        <div class="input-group">
                            <input type="text" name="s" class="form-control input-sm" placeholder="Buscar..." id="search-acessibilidade" />
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-success btn-sm" title="Buscar">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
                    
     <header>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
					<?php if ( get_header_image() ) : ?>
                        <h1>
                            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
                                <img src="<?php header_image(); ?>" class="img-responsive" alt="<?php bloginfo('name'); ?>">
                            </a>
                        </h1>
                    <?php else : ?>
                        <h1 class="sr-only"><?php bloginfo('name'); ?></h1>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="#inicio-menu" id="inicio-menu" class="sr-only">In&iacute;cio do Menu</a>
                <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-principal">
                        <span class="sr-only">Alternar Navega&ccedil;&atilde;o</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                    </div>

                    <div class="collapse navbar-collapse" id="menu-principal">
                    <?php
                        wp_nav_menu( array(
                            'menu'              => 'menu-principal',
                            'theme_location'    => 'menu-principal',
                            'depth'             => 2,
                            'container'         => false,
                            'container_class'   => 'collapse navbar-collapse',
                            'container_id'      => 'menu-principal',
                            'menu_class'        => 'nav navbar-nav',
                            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                            'walker'            => new wp_bootstrap_navwalker())
                        );
                    ?>
                    </div>
                </nav>
                <a href="#fim-menu" id="fim-menu" class="sr-only">Fim do Menu</a>
            </div>
        </div>
    </div>
    <a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do Conte&uacute;do</a>
