<?php

function custom_header_setup() {

	add_theme_support( 'custom-header', apply_filters( 'custom_header_args', array(
		'width'                  => 1260,
		'height'                 => 240,
		'flex-height'            => true,
		'admin-preview-callback' => 'admin_header_image',
	) ) );
}

add_action( 'after_setup_theme', 'custom_header_setup' );

function admin_header_image() {

?>

	<img src="<?php header_image(); ?>" alt="">

<?php
	}
?>