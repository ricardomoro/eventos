<?php
//aplica efeito lightbox nas imagens
add_filter('the_content', 'add_gallery_id_rel');
function add_gallery_id_rel($link) {
	global $post;
	return str_replace('<a href', '<a class="fancybox" rel="gallery1" href', $link);
}

add_filter('wp_get_attachment_link', 'add_gallery_id_rel2');
function add_gallery_id_rel2($link) {
	global $post;
	return str_replace('<a href', '<a class="fancybox" rel="gallery1" href', $link);
}