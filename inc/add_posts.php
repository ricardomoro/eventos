<?php

add_shortcode( 'posts', 'lista_post_shortcode' );
function lista_post_shortcode( $atts ) {
    ob_start();
    $query = new WP_Query( array(
        'posts_per_page' => 10,
    ) );
	
    if ( $query->have_posts() ) { ?>
	
	<?php while ( $query->have_posts() ) : $query->the_post(); ?>

		<div class="row">
    
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
    
                <?php if ( has_post_thumbnail()) :  ?>
                    <div style="width:140px; height:140px;float:left;margin: 0 1.75em 1em 0;">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail('thumbnail'); ?>
                        </a>
                    </div>
                <?php endif; ?>
    
            <?php the_excerpt(); ?>
       
            <a href='<?php get_permalink($getpost->ID); ?>'><em>Leia mais →</em></a>

        </div>

	<?php endwhile; wp_reset_postdata(); ?>

    <?php $lista_posts = ob_get_clean();
    return $lista_posts;
    }
}