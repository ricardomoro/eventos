<?php

/* inicia sidebar */

function blog_widgets_init() {
	
//widget "Sidebar"
	register_sidebar( array(
		'name' => 'Sidebar',
		'id' => 'sidebar',
		'description' => 'Sidebar',
		'before_widget' => ' <!--widget--><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '<div style="clear:both;"></div> </div><!--//widget--> ',
		'before_title'  => '<h2 class="title">',
		'after_title'   => '</h2> ',
		) );

//widget "Rodapé"
	register_sidebar( array(
		'name' => 'Rodapé',
		'id' => 'rodape',
		'description' => 'Rodape',
		'before_widget' => ' <!--widget--><div id="%1$s" class="widget %2$s">',
		'after_widget'  => '<div style="clear:both;"></div> </div><!--//widget--> ',
		'before_title'  => '<h2 class="title">',
		'after_title'   => '</h2> ',
		) );
	} 

//finaliza widget 
add_action( 'widgets_init', 'blog_widgets_init' );

?>