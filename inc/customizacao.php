<?php

function setup() {
add_theme_support( 'custom-background', apply_filters( 'custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );

}
add_action( 'after_setup_theme', 'setup' );

// Cor de fundo do template
require get_template_directory() . '/inc/custom-header.php';

function body_classes( $classes ) {

	if ( get_header_image() ) {
		$classes[] = 'header-image';
	} else {
		$classes[] = 'masthead-fixed';
	}
	return $classes;
}
add_filter( 'body_class', 'body_classes' );