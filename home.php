<?php get_header(); ?>

<?php breadcrumb(); ?>

<section class="container" id="conteudo">
    <div class="row">
    <?php while (have_posts()) : the_post(); ?>
        <article class="col-xs-12">
            <?php the_content(); ?>
        </article>
    <?php endwhile;?>
    </div>
</section>

<?php get_footer(); ?>
