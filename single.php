<?php get_header(); ?>

<?php breadcrumb(); ?>

<div class="container" id="conteudo">
	<section class="row">
		<?php if ( is_active_sidebar('sidebar') ) : ?>
            <article class="col-md-8">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title(); ?></h1>

                    <!-- Insere a imagem destaque na notícia, caso tenha. -->
                    <?php if ( has_post_thumbnail()): ?>
                        <div style="float:left;margin: 0 1.75em 2.5em 0;">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <a class="fancybox" rel="gallery1" href="<?php echo $url; ?>">
                                <?php the_post_thumbnail( array(320,225) ); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <!-- FIM - Insere a imagem destaque na notícia, caso tenha. -->

                    <?php the_content(); ?>
                <?php endwhile;?>
            </article>

            <aside class="col-md-4">
                <?php if ( ! dynamic_sidebar( 'sidebar' ) ) : endif; ?>
            </aside>
		<?php else : ?>
    		<article class="col-md-12">
                <?php while ( have_posts() ) : the_post(); ?>
                    <h1><?php the_title(); ?></h1>
                    
                    <!-- Insere a imagem destaque na notícia, caso tenha. -->
                    <?php if ( has_post_thumbnail()): ?>
                        <div style="float:left;margin: 0 1.75em 2.5em 0;">
                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <a class="fancybox" rel="gallery1" href="<?php echo $url; ?>">
                                <?php the_post_thumbnail( array(320,225) ); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <!-- FIM - Insere a imagem destaque na notícia, caso tenha. -->
                
                    <?php the_content(); ?>
                <?php endwhile; ?>
			</article>
		<?php endif; ?>
	</section>
</div>

<?php get_footer(); ?>
