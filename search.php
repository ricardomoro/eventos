<?php get_header(); ?>

<?php breadcrumb(); ?>

<div class="container" id="conteudo">
    <div class="row">
        <div class="col-xs-12">
            <section class="conteudo box">
            <?php if (have_posts()) : ?>
                <h2>Resultado pela busca de &ldquo;<?php the_search_query(); ?>&rdquo;</h2>
                <article id="post-<?php the_ID(); ?>">
                    <ol>
                    <?php while (have_posts()) : the_post(); ?>
                        <li style="list-style:none;">
                            <h3>
                                <a href="<?php the_permalink() ?>"  style="font-size:18px" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                                <?php the_title(); ?></a>
                            </h3>

                            <?php the_excerpt(); ?>
                        </li>
                    <?php endwhile; ?>
                    </ol>
                </article>
                <nav>
                    <p><?php posts_nav_link('&nbsp;&bull;&nbsp;'); ?></p>
                </nav>
            <?php else : ?>
                <article>
                    <h3>Não encontrado</h3>
                    <p>Desculpe, não foi encontrado o que você procura neste site.</p>
                </article>
            <?php endif; ?>
            </section>
        </div>
    </div>
</div>

<?php get_footer(); ?>
